package com.example.bruno.adivinha;

import android.view.View;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by bruno on 10/02/18.
 */
public class ButtonPlayListener implements View.OnClickListener {

    private final TextView textNumber;

    public ButtonPlayListener(TextView textNumber) {
        this.textNumber = textNumber;
    }

    @Override
    public void onClick(View view) {
        final int numeroAleatorio = new Random().nextInt(10);

        this.textNumber
                .setText("Numero escolhido: " + numeroAleatorio);
    }
}
